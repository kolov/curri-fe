import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MessagesComponent }      from './messages/messages.component';
import { AboutComponent }      from './about/about.component';
import { CiviComponent }      from './civi/civi.component';

const routes: Routes = [
{ path: 'messages', component: MessagesComponent },
{ path: 'about', component: AboutComponent },
{ path: '', component: CiviComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes, { useHash: true }) ]
})
export class AppRoutingModule { }
