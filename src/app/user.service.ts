import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import { MessageService } from './message.service';
import { User } from './user';


@Injectable()
export class UserService {


    constructor( private http: HttpClient,
             private messageService: MessageService )
    {
      this._user = <BehaviorSubject<User>>new BehaviorSubject({id: 'unknown', identity: null});
      this._user.next({id: 'unknown1', identity: null})
    }

  private _user: BehaviorSubject<User>;

  get user() {
    this.log('get user called')
    return this._user.asObservable();
  }

  loadUser()  {

   this.http.get<User>('/service/user')
   .pipe(
       tap(user => this.log('fetched user id=' + user.id)),
       catchError(this.handleError<User>('UserService.getUser', {id: 'unknown', identity: null}))
    )
    .subscribe( data => {
      this._user.next(data);
      this.log('called _user.next id=' + data.id)
    }, error => console.log('Could not load user.'))
  }

  logout() {
     this.http.post('/logout', {"dummy":"logout"}).subscribe(
        res => {
          this.loadUser();
        }, res => {
          this.loadUser();
        })
  }

  private log(message: string) {
    this.messageService.add('log UserService: ' + message);
  }


private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
    console.error(error); // log to console instead
    this.log(`${operation} failed: ${error}`);
    return of(result as T);
  };
}

}
