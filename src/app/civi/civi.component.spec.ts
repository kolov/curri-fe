import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CiviComponent } from './civi.component';

describe('CiviComponent', () => {
  let component: CiviComponent;
  let fixture: ComponentFixture<CiviComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CiviComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CiviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
