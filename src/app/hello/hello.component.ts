import{Component, OnInit, ViewEncapsulation}from '@angular/core';
import { User } from '../user';
import { UserService } from '../user.service';
import { MessageService } from '../message.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HelloComponent implements OnInit {

  private user : Observable<User>

  constructor(private userService: UserService, private messageService: MessageService ) { }

  ngOnInit() {
     this.user = this.userService.user
     this.userService.loadUser()
  }

  private log(message: string) {
    this.messageService.add('HelloComponent: ' + message);
  }

}
